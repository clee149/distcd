\documentclass[final,hyperref={pdfpagelabels=false}]{beamer}
\mode<presentation>
{
	% \usetheme{Berlin}
	\usetheme{CCGposter}
}

\usepackage[round,authoryear]{natbib}
\usepackage{times,tikz}
\usepackage{amsmath,amsthm, amssymb, latexsym,subfig}
\boldmath
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[orientation=landscape,size=a0,scale=1.4,debug]{beamerposter}
\usepackage{amsmath,enumerate,xspace,bbold,amsthm,mathtools}
\usepackage{multirow,array,theorem,amsmath,latexsym,xspace,bibentry}
\usepackage{array,theorem,amsmath,latexsym,hhline,pifont,xspace, bibentry}
\usepackage{grffile}
\usepackage{breakurl}
\usepackage{epstopdf}

\setbeamercolor{alerted text}{fg=red!80!yellow}
\definecolor{LightCyan}{rgb}{0.33,1,1}

%\usepackage[active,tightpage]{preview}
%\PreviewEnvironment{tikzpicture}
%\setlength\PreviewBorder{5pt}%
\usetikzlibrary{chains,fit,shapes}
%\usetheme{Madrid}
\renewcommand*{\thesubfigure}{}
\newcommand{\liblinear}{$\mbox{\href{http://www.csie.ntu.edu.tw/~cjlin/liblinear}{\bf LIBLINEAR}}$\xspace}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{arrows,automata,positioning}
\usetikzlibrary{matrix}

\setbeamerfont*{itemize/enumerate body}{size=\normalsize}
\setbeamerfont*{itemize/enumerate subbody}{parent=itemize/enumerate body}
\setbeamerfont*{itemize/enumerate subsubbody}{parent=itemize/enumerate body}

\DeclareMathOperator*{\argmin}{arg\,min}
\def\eps{{\sf epsilon}\xspace}
\def\uu{{\sf url}\xspace}
\newcommand{\tron}{$\mbox{{\sf TRON}}$\xspace}
\newcommand{\disdca}{$\mbox{{\sf DisDCA}}$\xspace}
\newcommand{\cocoa}{$\mbox{{\sf CoCoA}}$\xspace}
\newcommand{\bqoe}{$\mbox{{\sf BQO-E}}$\xspace}
\newcommand{\bqoa}{$\mbox{{\sf BQO-A}}$\xspace}
\newcommand{\bbqoe}{$\mbox{{\sf \bf BQO-E}}$\xspace}
\newcommand{\bbqoa}{$\mbox{{\sf \bf BQO-A}}$\xspace}
\newcommand{\mpi}{$\mbox{{\sf MPI-LIBLINEAR}}$\xspace}
\newcommand{\dsvm}{$\mbox{{\sf DSVM-AVE}}$\xspace}
\newcommand{\svmls}{$\mbox{{\sf DSVM-LS}}$\xspace}
\newcommand{\best}{\textbf}
\def\R{{ \mathbf{R}}}
\def\Z{{ \mathbf{Z}}}
\def\N{{ \mathbf{N}}}
\def\diag{{ \text{diag}}}
\def\ba{{\boldsymbol a}}
\def\bx{{\boldsymbol x}}
\def\by{{\boldsymbol y}}
\def\bv{{\boldsymbol v}}
\def\bw{{\boldsymbol w}}
\def\bu{{\boldsymbol u}}
\def\bs{{\boldsymbol s}}
\def\be{{\boldsymbol e}}
\def\bd{{\boldsymbol d}}
\def\b1{{\boldsymbol 1}}
\def\bzero{{\boldsymbol 0}}
\def\bxi{\boldsymbol \xi}
\def\AL{{\boldsymbol{\alpha}}}
\def\webspam{{\sf webspam}\xspace}
\def\sparkliblinear{{\bf Spark LIBLINEAR}\xspace}
\def\mpiliblinear{{\bf MPI LIBLINEAR}\xspace}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\graphicspath{{../figures/}}
\title{Distributed Box-Constrained Quadratic Optimization for Dual Linear SVM}
\author{Ching-pei Lee, Dan Roth}
\institute{University of Illinois at Urbana-Champaign}
%\date{Jul. 31th, 2007}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{document}
\tikzset{
very thick/.style= {line width=2.2pt},
arr/.style={draw opacity=1,
	decoration={markings,mark=at position 1
	with {\arrow[scale=2]{>}}},
	inner sep=\myinnersep,%
	outer sep=0,%
	minimum width=5mm,%
%    minimum height=\heightof{index1\nodepart{second}value1}+2*\myinnersep*1mm,%
%    minimum height=30mm,%
	postaction={decorate}, shorten >=0.4pt,
	}
}
\begin{frame}
	\begin{columns}[t]
		\begin{column}{.275\textwidth}
			\begin{block}{Background}
				\begin{itemize}
					\item Distributed linear classification with
						multiple machines: computation and
						I/O are fast.
					\item Bottleneck: \alert{communication} and
						\alert{syncronization}.
					\item Need methods with \alert{fewer rounds
						of communication}.
	\end{itemize}
\end{block}
\begin{block}{Linear SVM \citep{BB92a, VV95a}}
	\begin{itemize}
		\item Given $\{(\bx_i,y_i)\}_{i=1}^\ell$ $\bx_i \in \R^n$,
			$y_i = \pm 1$, $C>0$
\begin{equation}
	\label{eq:primal}
	\min\nolimits_{\bw\in \R^n} \quad f^P(\bw) \equiv
	\frac{1}{2}\bw^T\bw + C \sum_{i = 1}^\ell
	\xi(\bw,\bx_i;y_i),
\end{equation}
\begin{equation*}
	\xi(\bw,\bx_i;y_i) =
	\begin{cases}
		\max(0, 1 - y_i\bw^T \bx_i), &\text{ L1-loss SVM},\\
		\max(0, 1 - y_i\bw^T \bx_i)^2, &\text{ L2-loss SVM}.
	\end{cases}
\end{equation*}
	\item We solve the dual problem.
\begin{align}
	\min_{\AL \in \R^\ell}&\quad f(\AL) \equiv
	\frac{1}{2}\AL^T {\bar{Q}} \AL -
	\be^T \AL \nonumber \\
	\text{subject to}& \quad 0 \leq \alpha_i \leq U,
	i=1,\ldots,\ell,
	\label{eq:dual}
\end{align}
$\bar{Q} = Q + sI, Q = YX (YX)^T$, $I$ identity matrix,
\begin{equation*}
	(s, U) =
	\begin{cases}(0, C) &\text{ L1-loss SVM},\\
	(1/2C, \infty) &\text{ L2-loss SVM}.
\end{cases}
\end{equation*}
$X = [\bx_1,\ldots, \bx_\ell]^T$, $Y_{i,i} = y_i$ diagonal,
		$\be = [1,\ldots,1]^T$.
	\end{itemize}
\end{block}
	\begin{block}{Distributed environment}
		\begin{itemize}
				\item Data stored in $K$ machines:
					$X_{J_i}, Y_{J_i}$:
					sub matrices of $X, Y$.
				\item $\bigcup_{k=1}^K J_i = \{1,\ldots, \ell\}$,
					$J_i \cup J_k = \phi$, $\forall i \neq k$.
		\item Corresponding dual variables: $\AL =
			(\AL_{J_1},\ldots, \AL_{J_K})$.
	\end{itemize}
\end{block}

\begin{block}{Challenges}
	\begin{itemize}
		\item Each node only has \alert{partial information} of the data.
		\item Periodic \alert{expensive communications} are required to synchronize information of the whole data.
	\item Other methods: \alert{sub-linear}
		convergence for L1-loss SVM.
\end{itemize}
\end{block}
\begin{block}{Contributions}
\begin{itemize}
	\item First method with \alert{global linear
		convergence} ($O(\log(1/\epsilon))$ iterations)
		for distributed dual linear L1-loss SVM.
	\item Key: cheap \alert{line search} for dual variables' update, reducing the number of communication rounds needed.
	\item Training speed significantly better than other methods.
\end{itemize}
	\end{block}
\end{column}

\begin{column}{.335\linewidth}

	\begin{block}{Algorithm}
		\begin{itemize}
			\item At iteration $t$, given $\AL^t$,
				find update direction $\Delta \AL^t$ and
				step size $\eta_t$.
			\item Update $\AL^{t+1} \leftarrow \AL^t + \eta_t\Delta
				\AL^t$.
		\end{itemize}
	\end{block}
	\begin{block}
			{Compute $\Delta \AL^t$}
			\begin{itemize}
				\item
				All machines
				synchronize the same
				\begin{equation*}
					\bw^t = \sum\nolimits_{k=1}^K (Y_{J_k}
					X_{J_k})^T\AL^t_{J_k}
				\end{equation*}
			\item Use $\bw^t$ to obtain
				\begin{equation}
					\label{eq:grad}
				\nabla
				f(\AL^t)_{J_k} =
				Y_{J_k} X_{J_k} \bw^t + s\AL^t_{J_k} - \be.
			\end{equation}
			\item
				Pick some \alert{positive definite} $H$,
				solve
				\begin{equation}
					\Delta \AL^t = \argmin\nolimits_{\bd: \bzero \leq \AL^t + \bd
					\leq U \be}\quad \nabla f(\AL^t)^T \bd +
					\frac{1}{2} \bd^T H \bd
					\label{eq:H}
				\end{equation}
			\item Use \alert{block-diagonal} $H$, block $k$ uses
				only information from $X_{J_k}, Y_{J_k}$.
			$\Rightarrow$ \eqref{eq:H} can be
				solved \alert{without communication}: machine $k$ computes $\Delta \AL^t_{J_k}$.
			\item
				Let $\tilde{Q} = \begin{cases}
					\bx_i^T \bx_j y_i y_j &\text{ if $i,j$ in the same $J_k$}\\
					0 &\text{ otherwise}
				\end{cases}$,
	$\tilde{\tau} =
	\begin{cases}
		\tau & \text{ if L1-loss SVM}\\
		0 &\text{ if L2-loss SVM}
	\end{cases}$,
with a small constant $\tau > 0$ to ensure positive definiteness,
				we use
					$H =
	\tilde{Q} + (s+\tilde{\tau}) I$,
	\item But \alert{can use any other positive definite block-diagonal choices}.
		\end{itemize}
	\end{block}
	\begin{block}{Compute $\eta_t$ (I): Armijo backtracking line search}
		\begin{itemize}
			\item
				given $\beta, \sigma \in (0,1)$,
				$\eta_t  = \max_{k \in {0} \cup \N} \beta^k$ such that
\begin{equation}
	f(\AL^t + \beta^k \Delta \AL^t)- f(\AL^t) \leq
	\beta^k \sigma \nabla f(\AL^t)^T \Delta \AL^t
	\label{eq:armijo}
\end{equation}
\item \alert{Closed-form} solution by using \eqref{eq:dual}, \eqref{eq:grad} and taking
	logarithm.
	\begin{align}
		&\eta_t = \min(\beta^0, \beta^{\bar{k}}),\nonumber\\
	&\bar{k} \equiv \lceil \frac{1}{\log\beta}
	(\log \frac{2}{(\Delta \AL^t)^T\bar{Q}\Delta \AL^t}
	+ \log ( ( \sigma - 1 ) \nabla f (
		\AL^t )^T \Delta \AL^t
)) \rceil.\label{eq:simple-line}
\end{align}
		\end{itemize}
	\end{block}
	\begin{block}{Compute $\eta_t$ (II): Exact Solution}
			\begin{equation}
				\frac{\partial f(\AL + \eta_t \Delta
				\AL)}{\partial \eta_t} = 0 \Rightarrow
				\alert{\eta^*_t = \frac{- \nabla
					f(\AL^t)^T \Delta \AL^t }{(\Delta\AL^t)^T \bar{Q} \Delta
				\AL^t}}.
\label{eq:exact}
			\end{equation}
			\begin{itemize}
				\item Feasible solution:
						$\eta_t = \min(\eta^*, \max\{\eta \mid \bzero
					\leq \AL^t + \eta \Delta \AL^t \leq U\be\})$.
			\item Use {\em allreduce} to communicate across machines
				to get
				\begin{equation}
					\Delta \bw^t \equiv \sum\nolimits_{k=1}^K (Y_{J_k} X_{J_k})^T \Delta
					\AL^t_{J_k}.
					\label{eq:comm}
			\end{equation}
			\item For \eqref{eq:simple-line} and \eqref{eq:exact},
			$(\Delta \AL^t)^T \bar{Q} \Delta \AL^t = (\Delta
				\bw^t)^T \Delta \bw^t + s (\Delta \AL^t)^T
				\Delta \AL^t$
			\item
				\alert{No more communication} in updating $\bw^{t+1} = \bw^t + \eta_t
				\Delta \bw^t$.
			\end{itemize}
	\end{block}
\end{column}

\begin{column}{.37\linewidth}
	\begin{block}{Implementation \& Analysis}
		\begin{itemize}
		\item Dual solvers do not have stable primal value $\Rightarrow$
			pocket algorithm: maintain $\bw^t$ that has the smallest value in
			\eqref{eq:primal} so far as the current model.
		\item Solving \eqref{eq:H}: similar form to \eqref{eq:dual}.
		\alert{Can use any single-machine solver} for \eqref{eq:dual}.
			We use cyclic (with random shuffle) coordinate descent
			\citep{CJH08a}.
		\item Solving \eqref{eq:H} \alert{approximately}:
			can find another positive definite $H$ such that
			\begin{equation*}
				\Delta \AL^t = \argmin\nolimits_{\bd} \nabla f(\AL^t)^T \bd + \frac{1}{2}
				\bd^T H \bd.
			\end{equation*}
		\item One $O(n)$ communication per round in \eqref{eq:comm}.
			\item Leveraging framework of \cite{PT07a} and error bound in \cite{PWW13a},
				\alert{$O(\log(1/\epsilon))$} iterations for both L1-loss
				and L2-loss SVM is proven.
		\end{itemize}
	\end{block}
	\begin{block}{Experiments}
		Five implementations:
		\begin{itemize}
			\item \bqoe: our method + exact line search.
				\item \bqoa: our method + Armijo line search.
			\item \dsvm \citep{DP11a, MJ14a}: $H = \tilde{Q} + sI, \eta_t =
				1/K$.
			\item \disdca practical variant \citep{TBY13a, CM15a}: $H =
				K \tilde{Q} + sI, \eta_t = 1$.
			\item \tron \citep{YZ14a}: distributed Newton method for
				\eqref{eq:primal} with L2-loss.
		\end{itemize}

		\begin{itemize}
	\item	L1-loss SVM training time.
		\begin{center}
		\begin{tabular}{@{}l|r|r|rrrr@{}}
			Data set& $\ell$ & $n$ & \bbqoe& \bbqoa & \disdca & \dsvm\\
			\hline
			\webspam &280,000 &16,609,143  & $21.7$  &  30.6  &  54.7  &  79.5\\
			\uu & 1,916,904 & 3,231,961 & $1,092.9$  & 1,801.2  & 3,524.2  & 4,343.4\\
			\eps  & 400,000 & 2,000 &  $2.8$  &   3.9  &   8.0  &  13.2
		\end{tabular}
	\end{center}

\item	L2-loss SVM test accuracy\\
	\begin{center}
	\begin{tabular}{ccc}
		\webspam & \uu & \eps\\
		\includegraphics[width=4in]
		{../figures/fig2d.png} &
		\includegraphics[width=4in]
		{../figures/fig2b.png} &

		\includegraphics[width=4in]
		{../figures/fig-supp1d.png}
\end{tabular}
\end{center}

\item Speedup (training + IO time) of different number of machines for L2-loss SVM
	\begin{center}
	\begin{tabular}{cc}
		\webspam & \eps\\
		\includegraphics[width=4in]
		{../figures/fig3c.png}
	&
		\includegraphics[width=4in]
		{../figures/fig3d.png}
	\end{tabular}
\end{center}

	\end{itemize}
	
	\end{block}

	\begin{block}{Conclusions}
		\begin{itemize}
		\item An efficient
			algorithm for distributedly
			training linear SVM in the dual.
		\item Superior to state of the art
			both
			\alert{theoretically} and \alert{practically}.
		\item Implementation available in \url{http://cogcomp.cs.illinois.edu/page/software_view/DistSVM}.
		\end{itemize}
	\end{block}
	\end{column}
\end{columns}
\end{frame}
\bibliographystyle{plainnat}
\nobibliography{../local}
\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% End:
