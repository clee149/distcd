opt = 10364.4955632;
acc_opt = 99.0114;
data = 'webspam.tr';
method = {'bqoe';'bqoa';'disdca';'dsvm'};
loss = 'l1_loss';
xlab = 'Training Time (Seconds)';
ylab = 'Diff. to Opt. Dual Fun. Value';
loc = 'SouthWest';
figname = 'fig1c.png';
nodes = 16;
C = '1';
solver_rev = {'DSVM-AVE';'DisDCA';'BQO-A';'BQO-E'};
solver_rev_tron = {'TRON';'DSVM-AVE';'DisDCA';'BQOA';'BQOE'};
style = {'r-';'b--';'m:';'c-';'k^-'};
dual = []
primal = []
acc = []
iter = []
time = []

min_primal = inf;
best_acc = 0;

if (length(method) == 0 || length(data) == 0)
	return;
end

for k=1:length(method)
	filename = ['log/',data,'.',int2str(nodes),'nodes.',loss,'.c',C,'.',method{k},'.log']
	fid = fopen(filename,'r');
	if (fid == -1)
		break;
	end
	counter(k)=0;
	line = fgetl(fid);

	if (strcmp(method{k},'tron') == 0)
		while(ischar(line))
			t = strread(line,'%s','delimiter',' \t');
			if (length(t) > 0 && strcmp(t{1},'Iter'))
				counter(k) = counter(k) + 1;
				primal(k,counter(k)) = str2double(t{6});
				dual(k,counter(k)) = str2double(t{4});
				traintime(k,counter(k)) = str2double(t{8});
				acc(k,counter(k)) = str2double(t{10});
				if ((counter(k) > 1) && (primal(k,counter(k)) > min_primal))
					primal(k,counter(k)) = min_primal;
					acc(k,counter(k)) = best_acc;
				else
					min_primal = primal(k,counter(k));
					best_acc = acc(k,counter(k));
				end
				iter(k,counter(k)) = counter(k);
			end
			line = fgetl(fid);
		end
	else
		while(ischar(line))
			t = strread(line,'%s','delimiter',' \t');
			if (length(t) > 0 && strcmp(t{1},'Iter'))
				counter(k) = counter(k) + 1;
				iter(k,counter(k)) = str2double(t{2});
			elseif (length(t) > 0 && strcmp(t{1}, 'FUN'))
				primal(k,counter(k)) = str2double(t{2});
				acc(k,counter(k)) = str2double(t{4});
				traintimesec(k,counter(k)) = str2double(t{6});
			end
			line = fgetl(fid);
		end
	end
end

if (length(primal) > 0)
	primal = (primal - opt)/opt;
end

if (length(dual) > 0)
	dual = abs((abs(dual) - opt)/opt);
end

acc = (acc - acc_opt)/acc_opt;

h = figure;
set(gca,'fontsize',26);
i=length(style);

if (strcmp(xlab,'Training Time (Seconds)'))
	x_data = traintime(:,:);
else
	x_data = iter(:,:);
end

if (strcmp(ylab, 'Diff. to Opt. Dual Fun. Value'))
	y_data = dual;
elseif (strcmp(ylab, 'Diff. to Opt. Primal Fun. Value'))
	y_data = primal;
else
	y_data = acc;
end

if (strcmp(ylab, 'Relative Accuracy'))
	i=length(method);
	start_point = i;
	if (strcmp('tron',method{i}))
		i = i + 1;
		plot(x_data(i,2:counter(i)),y_data(i,2:counter(i)),tron_style,'LineWidth',4);
	else
		start_point = start_point - 1;
		plot(x_data(i,1:counter(i)),y_data(i,1:counter(i)),style{i},'LineWidth',4*i);
	end
	hold on;
	for i =start_point:-1:1
		plot(x_data(i,1:counter(i)),y_data(i,1:counter(i)),style{i},'LineWidth',4*i);
	end
else
	i=length(method);
	start_point = i;
	if (strcmp('tron',method{i}))
		i = i + 1;
		semilogy(x_data(i,2:counter(i)),y_data(i,2:counter(i)),tron_style,'LineWidth',4);
	else
		start_point = start_point - 1;
		semilogy(x_data(i,1:counter(i)),y_data(i,1:counter(i)),style{i},'LineWidth',4*i);
	end
	hold on;
	for i =start_point:-1:1
		semilogy(x_data(i,1:counter(i)),y_data(i,1:counter(i)),style{i},'LineWidth',4*i);
	end
end

xlabel(xlab,'FontSize',30);
ylabel(ylab,'FontSize',30);
if (length(method) == 5)
	le=legend(solver_rev_tron,'Location',loc);
else
	le=legend(solver_rev,'Location',loc);
end
set (le,'FontSize',30);
print(h,'-dpng',['fig/',figname]);
clear h;
