C = '1';
solver_rev_tron = {'TRON';'DSVM-AVE';'DisDCA';'BQO-E';'IO-TIME'};
solver_rev_tron_noio = {'TRON';'DSVM-AVE';'DisDCA';'BQO-E'};
style = {'c-';'r-';'b--';'m:';'k^-'};
traintime = [];
iotime = [0,0,0,0,0];
num_nodes = [1,2,4,8,16];


if (length(method) == 0 || length(data) == 0)
	return;
end

for i=1:length(num_nodes)
	nodes = num_nodes(i);
	for k=1:length(method)
		filename = ['log/',data,'.',int2str(nodes),'nodes.l2_loss.c',C,'.',method{k},'.log'];
		fid = fopen(filename,'r');
		if (fid == -1)
			break;
		end
		line = fgetl(fid);

		if (strcmp(method{k},'tron') == 0)
			while(ischar(line))
				t = strread(line,'%s','delimiter',' \t');
				if (length(t) > 0 && strcmp(t{13}, 'IO'))
					iotime(i) = iotime(i) + str2double(t{15});
				elseif (notreached == 1 && length(t) > 0 && strcmp(t{1},'Iter'))
					primal = str2double(t{6});
					if ((primal - opt)/opt < eps)
						traintime(k,i) = str2double(t{8});
						notreached = 0;
					end
				end
				line = fgetl(fid);
			end
		else
			while(ischar(line))
				t = strread(line,'%s','delimiter',' \t');
				if (length(t) > 0 && strcmp(t{13}, 'IO'))
					iotime(i) = iotime(i) + str2double(t{15});
				elseif (notreached == 1 && length(t) > 0 && strcmp(t{1}, 'FUN'))
					primal = str2double(t{2});
					if ((primal - opt) / opt < eps)
						traintime(k,i) = str2double(t{6});
						notreached = 0;
					end
				end
				line = fgetl(fid);
			end
		end
	end
end
io_time = io_time / length(method);

h = figure;
i = length(method);
semilogx(num_nodes, traintime(i,:),style{i+1});
hold on;
for i=length(method)-1:1
	semilogx(num_nodes, traintime(i,:),style{i+1}, 'LineWidth',3);
end
semilogx(num_nodes, io_time,style{1});

set(gca,'fontsize',26);
le = legend(solver_rev_tron, 'Location','NorthEast');
set(gca,'XTick',node);
xlim([1,16]);
xlabel('Number of machines','fontsize',30);
ylabel('Training Time (Seconds)','fontsize',30);
print(h,'-dpng',['fig/fig4',subfig_enum,'.png']);
clear h;

h = figure;
i = length(method);
tmp = traintime(i,:);
tmp = tmp./tmp(1);
semilogx(num_nodes, tmp, style{i+1},'LineWidth',3);
hold on;
for i=length(method)-1:1
	tmp = traintime(i,:);
	tmp = tmp./tmp(1);
	semilogx(num_nodes, tmp, style{i+1},'LineWidth',3);
end

set(gca,'fontsize',26);
le = legend(solver_rev_tron_noio, 'Location','NorthEast');
set(gca,'XTick',node);
xlim([1,16]);
xlabel('Number of machines','fontsize',30);
ylabel('Speedup','fontsize',30);
print(h,'-dpng',['fig/fig3',subfig_enum,'.png']);
clear h;

h = figure;
i = length(method);
tmp = traintime(i,:) + io_time;
tmp = tmp./tmp(1);
semilogx(num_nodes, tmp, style{i+1},'LineWidth',3);
hold on;
for i=length(method)-1:1
	tmp = traintime(i,:)+io_time;
	tmp = tmp./tmp(1);
	semilogx(num_nodes, tmp, style{i+1},'LineWidth',3);
end

set(gca,'fontsize',26);
le = legend(solver_rev_tron_noio, 'Location','NorthEast');
set(gca,'XTick',node);
xlim([1,16]);
xlabel('Number of machines','fontsize',30);
ylabel('Speedup','fontsize',30);
print(h,'-dpng',['fig/fig3',subfig_enum2,'.png']);
clear h;
