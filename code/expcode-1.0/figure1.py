from  parameter import *
import os
from os import system,popen

xlab = 'Training Time (Seconds)'
ylab = 'Diff. to Opt. Dual Fun. Value'

nodes = '16nodes'
methodlist = ['bqoe','bqoa','disdca','dsvm']
data = ['url','webspam']
losslist = ['l1_loss','l2_loss']
c = 1
fig_enum = ['a','b','c','d']

for d in data:
    for loss in losslist:
        for method in methodlist:
            dp = log_path +  data_path[d] + '.' + nodes +'.' + loss + '.c%s.'%c + method +  '.log'
            print dp

            try:
                tmp_data = open(dp,'r').readlines()
            except:
                traindata = path + data_path[d]
                testdata = path + test_path[d]
                cmd = "mpirun -hostfile %s %s -c %s -f %s -s %s -e %s %s %s >> %s"%('machinelist', train_exe, c, nfs, solver[loss][method], data_eps[d][c], traindata,testdata, dp)
                system('echo \'%s\' >> %s'%(cmd, dp))
                system(cmd)
                tmp_data = open(dp,'r').readlines()

t = 0
for d in data:
    for loss in losslist:
        filename = "fig1"+fig_enum[t]
        p = open(filename+".m","w")
        print >> p, 'opt = %s;'%opt_val[loss][c][d]
        print >> p, 'acc_opt = %s;'%opt_acc[loss][c][d]
        print >> p, "data = '%s';"%data_path[d]
        print >> p, "method = {'bqoe';'bqoa';'disdca';'dsvm'};"
        print >> p, "loss = '%s';"%loss
        print >> p, "xlab = '%s';"%xlab
        print >> p, "ylab = '%s';"%ylab
        print >> p, "loc = 'SouthWest';"
        print >> p, "figname = 'fig1%s.png';"%fig_enum[t]
        if ('Accuracy' in ylab):
            print >> p, 'yleft = %s;'%yleft[loss][c][d]
        t = t + 1
        p.close()
        cmd = 'cat draw.m >> %s.m'%filename
        os.system(cmd)
        cmd = "matlab -nodisplay -nodesktop -r \"%s;exit\""%filename
        print cmd
        system(cmd)

