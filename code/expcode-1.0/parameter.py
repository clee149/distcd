train_exe = 'mpi-liblinear/train'
nfs = 1
c_set = [1, 1e-2, 1e-4]
path = "../data/"
solver = {'l1_loss':{'bqoe':0,'bqoa':1,'disdca':2,'dsvm':3},'l2_loss':{'bqoe':4,'bqoa':5,'disdca':6,'dsvm':7,'tron':8}}
log_path = "log/"
data_path = {"url":"url.tr","webspam":"webspam.tr","epsilon":"epsilon_normalized"}
test_path = {"url":"url.t","webspam":"webspam.t","epsilon":"epsilon_normalized.t"}
output_name = {"news20":"\\news20","mnist":"\\mnist","sector":"\\sector","rcv1":"\\rcv1"}
data_eps = {"epsilon":{1:0.01,1e-2:0.01,1e-4:0.01},"webspam":{1:0.01,1e-2:0.01,1e-4:0.01},"url":{1:0.3,1e-2:0.01,1e-4:0.01}}
data_dual_eps = {"epsilon":{1:0.01,1e-2:0.01,1e-4:0.01},"webspam":{1:0.01,1e-2:0.01,1e-4:0.01},"url":{1:0.04,1e-2:0.01,1e-4:0.01}}

opt_val = {'l1_loss':{1:{"url":9.39233972023534806794e+03 ,"webspam":1.03644955632104993128e+04 ,"epsilon":1.096914134795929e+05}, 1e-2:{"url":7.33022625659280492982e+02 ,"webspam":4.79186774824334236200e+02 ,"epsilon":2.15696934992820706611e+03}, 1e-4:{"url":1.71370809109260946457e+01 ,"webspam":1.74740320164600717590e+01 ,"epsilon":3.93113492117882898924e+01 }}, 'l2_loss': {1:{"url":7.94508587952736615989e+03 ,"webspam":9.22158808610316009435e+03 ,"epsilon":1.31303806193538097432e+05 }, 1e-2:{"url":7.66162727973628875588e+02 ,"webspam":4.27239317291419865796e+02 ,"epsilon":1.91350179622278710667e+03}, 1e-4:{"url":1.82470949126762178594e+01 ,"webspam":1.52934662911359424697e+01 ,"epsilon":3.78080227232598673481e+01}}}

opt_acc = {'l1_loss':{1:{"url":99.5374,"webspam":99.0114,"epsilon":89.761}, 1e-2:{"url":98.619,"webspam":95.9214,"epsilon":88.032}, 1e-4:{"url":96.9417,"webspam":78.4386,"epsilon":66.678}}, 'l2_loss': {1:{"url":99.5395,"webspam":99.3214,"epsilon":89.75}, 1e-2:{"url":98.8853,"webspam":97.2771,"epsilon":89.007}, 1e-4:{"url":97.6366,"webspam":90.4314,"epsilon":76.507}}}
