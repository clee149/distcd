#!/bin/bash
cd data
wget http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/epsilon_normalized.bz2
wget http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/epsilon_normalized.t.bz2
wget http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/url_combined.bz2
wget http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/webspam_wc_normalized_trigram.svm.bz2

for i in *.bz2
do
	bunzip2 $i
done

python ../tools/subset.py url_combined 1916904 url.tr url.t
python ../tools/subset.py webspam_wc_normalized_trigram.svm 280000 webspam.tr webspam.t

rm *.bz2 url_combined webspam_wc_normalized_trigram.svm

for i in *
do
	python ../tools/split.py -N 16 -f 1 ../machinelist $i
done
