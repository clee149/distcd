from parameter import *
from parse_digit import *
from os import system
fig_enum = ['a','b']
fig_enum2 = ['c','d']
c = 1
loss = 'l2_loss'
data = ['epsilon','webspam']
methodlist = ['bqoe','disdca','dsvm','tron']

for (i,d) in enumerate(data):
    for method in methodlist:
        dp = log_path +  data_path[d] + '.' + nodes +'.' + loss + '.c%s.'%c + method +  '.log'
        try:
            tmp_data = open(dp,'r').readlines()
        except:
            traindata = path + data_path[d]
            testdata = path + test_path[d]
            cmd = "mpirun -hostfile %s %s -c %s -f %s -s %s -e %s %s %s >> %s"%('machinelist', train_exe, c, nfs, solver[loss][method], data_eps[d][c], traindata,testdata, dp)
            system('echo \'%s\' >> %s'%(cmd, dp))
            system(cmd)
            tmp_data = open(dp,'r').readlines()

    filename = "speedup-"+d
    p = open(filename+".m","w")
    print >> p, 'opt = %s;'%opt_val[loss][c][d]
    print >> p, 'eps = %s;'%data_eps[d][c]
    print >> p, "data = '%s';"%data_path[d]
    print >> p, "subfig_enum = %s;"%fig_enum[i]
    print >> p, "subfig_enum2 = %s;"%fig_enum2[i]
    p.close()
    cmd = 'cat speedup.m >> %s.m'%filename
    os.system(cmd)
    cmd = "matlab -nodisplay -nodesktop -r \"%s;exit\""%filename

