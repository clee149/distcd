from parameter import *
from parse_digit import *
from os import system

f = open('machinelist','r').readlines()
n_node = len(f)
close(f)
nodes = int2str(n_node) + 'nodes'
#remove those method/data/criterion you are not interested in its result
methodlist = ['qboe','qboa','disdca','dsvm','tron']
data = ['url','epsilon','webspam']

losslist = ['l1_loss','l2_loss']
c_list=[1,1e-2,1e-4]

for c in c_list:
	solver_len = [len(methodlist)]*2
	print nodes, 'C = %s\\\\'%c
	print "Primal Time\\\\"
	if 'tron' in methodlist:
		solver_len[0] = solver_len[0] - 1
	print "\\begin{tabular}{l|c|"+"r"*solver_len[0] +"|" + "r"*solver_len[1] +"}"
	print "\\multirow{2}{*}{Data set}&& \\multicolumn{ %s }{|c}{L1-SVM solvers} & \\multicolumn{ %s }{|c}{L2-SVM solvers} \\\\"%(solver_len[0], solver_len[1])
	print "&$\epsilon$",
	for i in solver_len:
		for j in xrange(i):
			print "&" + methodlist[j],
	print "\\\\"
	print "\\hline"

	all_time_primal = {'l1_loss':[],'l2_loss':[]}
	all_iter_primal = {'l1_loss':[],'l2_loss':[]}
	all_time_dual = {'l1_loss':[],'l2_loss':[]}
	all_iter_dual = {'l1_loss':[],'l2_loss':[]}
	primal_data_time = {}
	primal_data_iter = {}
	dual_data_time = {}
	dual_data_iter = {}
	for d in data:
		all_time_primal = {'l1_loss':[],'l2_loss':[]}
		all_iter_primal = {'l1_loss':[],'l2_loss':[]}
		all_time_dual = {'l1_loss':[],'l2_loss':[]}
		all_iter_dual = {'l1_loss':[],'l2_loss':[]}
		for i in xrange(len(solver_len)):
			for method_count in xrange(solver_len[i]):
				method = methodlist[method_count]
				loss = losslist[i]
				primal_done = 0
				dual_done = 0

				dp = log_path +  data_path[d] + '.' + nodes +'.' + loss + '.c%s.'%c + method +  '.log'
				try:
					tmp_data = open(dp,'r').readlines()
				except:
					traindata = path + data_path[d]
					testdata = path + test_path[d]
					cmd = "mpirun -hostfile %s %s -c %s -f %s -s %s -e %s %s %s >> %s"%('machinelist', train_exe, c, nfs, solver[loss][method], data_eps[d][c], traindata,testdata, dp)
					#system('echo \'%s\' >> %s'%(cmd, dp))
					#system(cmd)
					tmp_data = open(dp,'r').readlines()
				iters = 0
				opt = opt_val[loss][c][d]
				if (method != 'tron'):
					for l in tmp_data:
						if 'Iter' in l:
							iters = iters + 1
							tokens = l.split(' ')
							time = float(tokens[7])
							primal = float(tokens[5])
							dual = abs(float(tokens[3]))
							if primal_done == 0 and ((primal - opt)/opt < data_eps[d][c]):
								all_time_primal[loss].append(time)
								all_iter_primal[loss].append(iters)
								primal_done = 1
							if dual_done == 0 and abs((dual- opt)/opt) < data_dual_eps[d][c]:
								all_time_dual[loss].append(time)
								all_iter_dual[loss].append(iters)
								dual_done = 1
							if (primal_done == 1 and dual_done == 1):
								break;
					if (primal_done != 1):
						all_time_primal[loss].append(float('inf'))
						all_iter_primal[loss].append(float('inf'))
					if (dual_done != 1):
						all_time_dual[loss].append(float('inf'))
						all_iter_dual[loss].append(float('inf'))
				else:
					for l in tmp_data:
						if 'Iter' in l:
							iters = int(l.split(' ')[1])
						elif 'FUN' in l:
							tokens = l.split(' ')
							time = float(tokens[5])
							primal = float(tokens[1])
							if (primal - opt)/opt < data_eps[d][c]:
								all_time_primal[loss].append(time)
								all_iter_primal[loss].append(iters)
								primal_done = 1
								break
					if (primal_done != 1):
						all_time_primal[loss].append(float('inf'))
						all_iter_primal[loss].append(float('inf'))
			primal_data_time[d] = all_time_primal
			primal_data_iter[d] = all_iter_primal
			dual_data_time[d] = all_time_dual
			dual_data_iter[d] = all_iter_dual

	for d in data:
		print '%s & %s'%(d, data_eps[d][c]),
		for i in xrange(len(solver_len)):
			loss = losslist[i]
			best_time = min(primal_data_time[d][losslist[i]])
			for j in xrange(solver_len[i]):
				time = primal_data_time[d][loss][j]
				digit = FormatWithCommas("%5.1f",float(time))
				if time == best_time:
					print ' &{\\bf %s}'%digit,
				elif time == float('inf'):
					print ' & NA',
				else:
					print ' & %s'%digit,
		print '\\\\'
	print "\\end{tabular}"

	print "Primal Iterations\\\\"
	solver_len = [len(methodlist)]*2
	if 'tron' in methodlist:
		solver_len[0] = solver_len[0] - 1
	print "\\begin{tabular}{l|c|"+"r"*solver_len[0] +"|" + "r"*solver_len[1] +"}"
	print "\\multirow{2}{*}{Data set}&& \\multicolumn{ %s }{|c}{L1-SVM solvers} & \\multicolumn{ %s }{|c}{L2-SVM solvers} \\\\"%(solver_len[0], solver_len[1])
	print "&$\epsilon$",
	for i in solver_len:
		for j in xrange(i):
			print "&" + methodlist[j],
	print "\\\\"
	print "\\hline"
	for d in data:
		print '%s & %s'%(d, data_eps[d][c]),
		for i in xrange(len(solver_len)):
			loss = losslist[i]
			best_iter = min(primal_data_iter[d][losslist[i]])
			for j in xrange(solver_len[i]):
				iters = primal_data_iter[d][loss][j]
				digit = FormatWithCommas("%d",int(iters))
				if iters == best_iter:
					print '&{\\bf %s}'%digit,
				elif iters == float('inf'):
					print '& NA',
				else:
					print '& %s'%digit,
		print '\\\\'
	print "\\end{tabular}"

	print "Dual Time\\\\"
	solver_len = [len(methodlist)]*2
	if 'tron' in methodlist:
		solver_len[0] = solver_len[0] - 1
		solver_len[1] = solver_len[1] - 1
	print "\\begin{tabular}{l|c|"+"r"*solver_len[0] +"|" + "r"*solver_len[1] +"}"
	print "\\multirow{2}{*}{Data set}&& \\multicolumn{ %s }{|c}{L1-SVM solvers} & \\multicolumn{ %s }{|c}{L2-SVM solvers} \\\\"%(solver_len[0], solver_len[1])
	print "&$\epsilon$",
	for i in solver_len:
		for j in xrange(i):
			print "&" + methodlist[j],
	print "\\\\"
	print "\\hline"


	for d in data:
		print '%s & %s'%(d, data_dual_eps[d][c]),
		for i in xrange(len(solver_len)):
			loss = losslist[i]
			best_time = min(dual_data_time[d][losslist[i]])
			for j in xrange(solver_len[i]):
				time = dual_data_time[d][loss][j]
				digit = FormatWithCommas("%5.1f",float(time))
				if time == best_time:
					print ' &{\\bf %s}'%digit,
				elif time == float('inf'):
					print ' & NA',
				else:
					print ' & %s'%digit,
		print '\\\\'
	print "\\end{tabular}"

	print "Dual Iterations\\\\"
	solver_len = [len(methodlist)]*2
	if 'tron' in methodlist:
		solver_len[0] = solver_len[0] - 1
		solver_len[1] = solver_len[1] - 1
	print "\\begin{tabular}{l|c|"+"r"*solver_len[0] +"|" + "r"*solver_len[1] +"}"
	print "\\multirow{2}{*}{Data set}&& \\multicolumn{ %s }{|c}{L1-SVM solvers} & \\multicolumn{ %s }{|c}{L2-SVM solvers} \\\\"%(solver_len[0], solver_len[1])
	print "&$\epsilon$",
	for i in solver_len:
		for j in xrange(i):
			print "&" + methodlist[j],
	print "\\\\"
	print "\\hline"
	for d in data:
		print '%s & %s'%(d, data_dual_eps[d][c]),
		for i in xrange(len(solver_len)):
			loss = losslist[i]
			best_iter = min(dual_data_iter[d][losslist[i]])
			for j in xrange(solver_len[i]):
				iters = dual_data_iter[d][loss][j]
				digit = 'NA'
				if (iters != float('inf')):
					digit = FormatWithCommas("%d",int(iters))
				if iters == best_iter:
					print '&{\\bf %s}'%digit,
				else:
					print '& %s'%digit,
		print '\\\\'
	print "\\end{tabular}"
